package practice;

import java.util.ArrayList;

public class PartChooseOptions extends Object implements Values{
	String ID;
	ArrayList<String> choices;
	ArrayList<Integer> distance; 
	ArrayList<String> availability;
	ArrayList<String> partID;
	ArrayList<Integer> number;
		
	public PartChooseOptions(String id, ArrayList<String> choices, ArrayList<Integer> distance, ArrayList<String> availability, ArrayList<String> partId, ArrayList<Integer> number) {
		this.ID = id;
		this.choices = choices;
		this.distance = distance;
		this.availability = availability;
		this.partID = partId;
		this.number = number;
	}
	public String getID()					{return ID;}
	public ArrayList<String> getChoices()	{return choices;}
	public ArrayList<Integer> getDistance()	{return distance;}
	public ArrayList<String> getAvail()		{return availability;}
	public ArrayList<String> getPartID()	{return partID;}
	public ArrayList<Integer> getNumber()	{return number;}
	

}
