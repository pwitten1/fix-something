package practice;

import java.util.ArrayList;

public class PartAnt extends Object implements Values{
	String ID;
	ArrayList<String> qr;
	double latitude;
	double longitude;
	
	
	public PartAnt(String id, ArrayList<String> q, double lat, double lng)
	{
		ID = id;
		qr = q;
		latitude = lat;
		longitude = lng;
	}
		
	public String getID()				{return ID;		}
	public ArrayList<String> getQr()	{return qr;		}
	public double getLat()				{return latitude;	}
	public double getLng()				{return longitude;	}
	
}