package practice;

import java.io.ByteArrayInputStream;
import java.io.BufferedInputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.awt.image.*;
import javax.imageio.ImageIO;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import org.json.*;
import sun.misc.BASE64Decoder;
import com.google.gson.Gson;

@Path("/start")
public class start implements Values {
	File file = new File("C:/Users/pdw2/workspace4/practice/object1.txt");
	Gson gson = new Gson();
	String CODE;

	@POST
	public JSONObject hello(String json) {
		JSONObject stuff = null;
		JSONObject send = null;
		try {
			stuff = new JSONObject(json);
			send = new JSONObject();
			CODE = stuff.getString(Values.CODE);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean conf;
		try {
			switch (CODE) {
			case USER_AUTH:
				validate check = new validate();
				conf = check.valid(stuff.getString(CODE_ID),
						stuff.getString(CODE_USER));
				send.put(CODE_USER, stuff.getString(CODE_USER));
				send.put(CODE_ID, stuff.getString(CODE_ID));
				send.put(CODE_CONFIRM, conf);
				break;
			case CHECK_PART:
				PartAnt pAnt = new Gson().fromJson(stuff.getString(CODE_DATA),
						PartAnt.class);
				Options optpart = new Options(pAnt.getID());
				for (int w = 0; w < pAnt.getQr().size(); w++) {
					CheckPart retrieve = new CheckPart(pAnt.getID());
					PartChooseOptions optionchoice = new PartChooseOptions(
							pAnt.getID(), retrieve.getchoices(),
							retrieve.getDistance(pAnt.getLat(), pAnt.getLng()),
							retrieve.getavailability(), retrieve.getPartID(pAnt
									.getQr().get(w)), retrieve.getnum());
					optpart.addPartOption(optionchoice);
				}
				send.put(CODE_DATA, new Gson().toJson(optpart));
				ArrayList<String> Qrcodes = pAnt.getQr();
				ArrayList<WriteFile> fileholder = new ArrayList<WriteFile>();
				try {
					ObjectInputStream obin = new ObjectInputStream(
							new BufferedInputStream(new FileInputStream(file)));
					ArrayList<WriteFile> other = (ArrayList<WriteFile>) obin
							.readObject();
					fileholder.addAll(other);
					obin.close();
				} catch (EOFException e) {
				}
				for (int a = 0; a < Qrcodes.size(); a++) {
					WriteFile reader = new WriteFile(pAnt.getID(), pAnt.getQr()
							.get(a));
					fileholder.add(reader);
				}
				ObjectOutputStream objectOut = new ObjectOutputStream(
						new FileOutputStream(file));
				objectOut.writeObject(fileholder);
				objectOut.close();
				break;
			case CHECK_MAINTENANCE:
				MainAnt mAnt = new Gson().fromJson(stuff.getString(CODE_DATA),
						MainAnt.class);
				ArrayList<String> Stringpic = new ArrayList<String>();
				Stringpic = mAnt.getPics();
				try {
					System.out.println("this is test 2");
					for (int q = 0; q < Stringpic.size(); q++) {
						String timeStamp = new SimpleDateFormat(
								"yyyMMdd_HHmmss").format(new Date());
						ImageIO.write(stringToBitMap(Stringpic.get(q)), "jpg",
								new File("C:/Users/pdw2/Pictures/image"
										+ timeStamp + q + ".jpg"));
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
				CheckMain main = new CheckMain(mAnt.getID());
				ArrayList<Integer> dist = main.getdistance(mAnt.getLat(),
						mAnt.getLng());
				ArrayList<String> avail;
				if (mAnt.getAvail() == null) {
					avail = main.getAvailability("STOP");
				} else {
					avail = main.getAvailability(mAnt.getAvail());
				}
				ArrayList<String> choices = main.getChoices();
				MainChooseOptions optionchoice1 = new MainChooseOptions(
						mAnt.getID(), choices, dist, avail);
				Options opt = new Options(mAnt.getID());
				opt.addMainOption(optionchoice1);
				send.put(CODE_DATA, new Gson().toJson(opt));
				// find maintenance
				break;
			case SELECT_PART:
				send.put(CODE_ID, stuff.get(CODE_ID));
				send.put(CODE_CONFIRM, true);
				break;
			case SELECT_MAINTENANCE:
				send.put(CODE_ID, stuff.get(CODE_ID));
				send.put(CODE_CONFIRM, true);
				break;
			case UPDATE:
				Data back = new Data(stuff.getString(CODE_ID));
				boolean tf = stuff.getBoolean("CODE_CONFIRM");
				try {
					ObjectInputStream objectInput = new ObjectInputStream(
							new BufferedInputStream(new FileInputStream(file)));
					ObjectOutputStream obout = new ObjectOutputStream(
							new FileOutputStream(file));
					ArrayList<WriteFile> reader1 = (ArrayList<WriteFile>) objectInput
							.readObject();
					ArrayList<Integer> tbd = new ArrayList<Integer>();
					for (int w = 0; w < reader1.size(); w++) {
						if (reader1.size() == 0) {
							System.out.println("null");
						} else if (reader1.get(w).getUID()
								.equals(stuff.getString(CODE_ID))) {
							String mess= reader1.get(w).getMess();
							if (mess.equals(reader1.get(w).getPID()+ " has arrived. It has now been removed from your queue")) {
								String timeStamp = new SimpleDateFormat(
										"yyyMMdd_HHmmss").format(new Date());
								back.add(reader1.get(w).getPID(), mess + "\n" + timeStamp);
								reader1.get(w).wipe();
								System.out.println("wipe");
								tbd.add(w);
							} else {
								if (tf) {
									String neu = reader1.get(w).getMess();
									String timeStamp = new SimpleDateFormat(
											"yyyMMdd_HHmmss")
											.format(new Date());
									back.add(reader1.get(w).getPID(), neu
											+ "\n"
											+ timeStamp);
									
									if (neu.equals(reader1.get(w).getPID()
											+ " has arrived. It has now been removed from your queue")) {
										reader1.get(w).wipe();
										tbd.add(w);
									}
								}
							}
						}
						
					}
					for (int r = 0; r < tbd.size(); r++) {
							reader1.remove(tbd.get(r));
							System.out.println("delete");
						}
					objectInput.close();
					obout.writeObject(reader1);
					obout.close();

				} catch (EOFException e) {
					System.out.println("There is nothing on the file");
				} catch (NullPointerException ex) {
					System.out.println("There is nothing on the file");
				}
				send.put(CODE_DATA, new Gson().toJson(back));
				// read file listing of those with the id, output the parts
				// acquired by that id, as well as there delivery status
				break;
			default:
				// error
				break;
			}
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return send;
	}

	public BufferedImage stringToBitMap(String s) {
		BufferedImage image = null;
		try {
			byte[] imageByte;
			BASE64Decoder decoder = new BASE64Decoder();
			imageByte = decoder.decodeBuffer(s);
			ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
			bis.close();
			image = ImageIO.read(bis);
		} catch (Exception e) {
			// error
			System.out.println("Is there a catch?");
		}
		return image;
	}

}
