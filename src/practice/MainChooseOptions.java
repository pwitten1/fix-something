package practice;

import java.util.ArrayList;

public class MainChooseOptions extends Object implements Values{
	String ID;
	ArrayList<String> choices;
	ArrayList<Integer> distance; 
	ArrayList<String> availability;
	
		
	public MainChooseOptions(String id, ArrayList<String> choices, ArrayList<Integer> distance, ArrayList<String> availability) {
		this.ID = id;
		this.choices = choices;
		this.distance = distance;
		this.availability = availability;
	}
	public String getID()					{return ID;}
	public ArrayList<String> getChoices()	{return choices;}
	public ArrayList<Integer> getDistance()	{return distance;}
	public ArrayList<String> getAvail()		{return availability;}
	

}
