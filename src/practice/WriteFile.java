package practice;

import java.io.Serializable;
import java.util.Random;

public class WriteFile implements Serializable {
	// true = everything
	public String UID, PID;

	public WriteFile(String U, String P) {
		UID = U;
		PID = P;

	}

	public String getUID() {
		return UID;
	}

	public String getPID() {
		return PID;
	}

	public String getMess() {
		Random gen = new Random();
		int a = gen.nextInt(10);
		if (a < 6) {
			return this.getPID()
					+ " has arrived. It has now been removed from your queue";
		} else {
			return this.getPID() + "is on the way";
		}
	}
	public void wipe() {
		UID = null;
		PID = null;
	}

}
