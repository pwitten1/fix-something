package practice;

import java.util.ArrayList;

public class Options extends Object implements Values{
	private String ID;
	private ArrayList<PartChooseOptions> parts;
	private ArrayList<MainChooseOptions> mains;
	public Options(String id) {
		ID = id;
	}
	
	public String getID()	{return ID;}
	public ArrayList<PartChooseOptions> getPartOptions()	{return parts;}
	public ArrayList<MainChooseOptions>	getMainOptions()	{return mains;}
	
	public void addPartOption(PartChooseOptions p)	{if (parts == null) parts = new ArrayList<PartChooseOptions>(); parts.add(p);}
	public void addMainOption(MainChooseOptions m)	{if (mains == null) mains = new ArrayList<MainChooseOptions>();mains.add(m);}

}
