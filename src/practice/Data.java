package practice;

import java.util.ArrayList;

public class Data extends Object implements Values{
	String ID;
	ArrayList<String> messages;
	ArrayList<String> partID;
	public Data(String id) {
		messages = new ArrayList<String>();
		partID = new ArrayList<String>();
		ID = id;
	}
	public String getID()					{return ID;}
	public ArrayList<String> getMessages()	{return messages;}
	public ArrayList<String> getPartID()	{return partID;}
	
	public void remove(String p, String m)
	{
		for (int i =0; i <= messages.size();i++)
		{
			if (m.equals(messages.get(i)) && p.equals(partID.get(i)))
			{
				messages.remove(i);
				partID.remove(i);
				i--;
			}
		}
	}
	public void remove(int i)
	{
		messages.remove(i);
		partID.remove(i);
	}
	public void merge(Data data)
	{
		if (partID == null)partID = new ArrayList<String>();
		if (messages == null) messages = new ArrayList<String>();
		for (int i= 0; i < data.getMessages().size(); i++)
		{
			boolean add = true;
			for (int j =0; j<messages.size();j++)
			{
				if (data.getMessages().get(i).equals(messages.get(i)) && data.getPartID().get(i).equals(partID.get(i))) add = false;
			}
			
			if (add)
			{
				messages.add(data.getMessages().get(i));
				partID.add(data.getPartID().get(i));
			}
		}
	}
	public void add(String p, String m)
	{
		if (partID == null)partID = new ArrayList<String>();
		if (messages == null) messages = new ArrayList<String>();
		partID.add(p);
		messages.add(m);
	}
}
