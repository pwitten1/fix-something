package practice;

import java.util.ArrayList;
import java.util.Random;

public class CheckMain {
	Random gen = new Random();
	public String ID;
	public ArrayList<String> Choices = new ArrayList<String>();
	public ArrayList<String> Availability = new ArrayList<String>();
	public ArrayList<Integer> distance = new ArrayList<Integer>();
	public int count;
	public String[] names = { "Bill", "Steve", "Jim", "Pete", "Bob", "Joe",
			"Ben" };

	public CheckMain(String id) {
		ID = id;
		int a = 0;

		do {
			a = gen.nextInt(10);
			count++;
		} while (a < 7);
	}

	public ArrayList<String> getAvailability(String ava) {
		 if(ava.equals("STOP")){ 
			 //error
		 }else{
		}
		int t;
		for (int w = 0; w < count; w++) {
			t = gen.nextInt(8) + 10;
			Availability.add("Estimated maintenance date: " + t + " days");
		}
		return Availability;
	}

	public ArrayList<Integer> getdistance(double lat, double lon) {

		int radius = 3959;
		for (int f = 0; f < count; f++) {
			int a = gen.nextInt(10);
			int b = gen.nextInt(10);
			double dlat = Math.toRadians(a);
			double dlon = Math.toRadians(b);
			double d = Math.sin(dlat / 2) * Math.sin(dlat / 2)
					+ Math.cos(Math.toRadians(lat))
					* Math.cos(Math.toRadians(lat + a)) * Math.sin(dlon / 2)
					* Math.sin(dlon / 2);
			double c = 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d));
			double dist = radius * c;
			distance.add((int) dist);
		}
		return distance;
	}

	public ArrayList<String> getChoices() {
		for (int a = 0; a < count; a++) {
			Choices.add(names[gen.nextInt(7)]);
		}
		return Choices;
	}

}
