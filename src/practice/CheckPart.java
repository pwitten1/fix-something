package practice;

import java.util.ArrayList;
import java.util.Random;

public class CheckPart implements Values {

	Random gen = new Random();
	String[] options = { "Best Buy", "Wal-Mart", "Costco", "Home Depot",
			"Lowes", "Sears", "Macy's" };
	String ID;
	ArrayList<String> choices = new ArrayList<String>();
	// name of distributor

	ArrayList<Integer> distance = new ArrayList<Integer>();

	ArrayList<String> availability = new ArrayList<String>();
	// days until delivery: expected

	ArrayList<String> partID = new ArrayList<String>();
	// not all part ids are the same, some might be different if different kinds
	// of parts work, or if there
	// is an incremental difference between parts that has no bearing on it
	// effectiveness, e.g. different colors
	ArrayList<Integer> number = new ArrayList<Integer>();

	public int count = 0;

	public CheckPart(String id) {
		ID = id;
		int a = 0;

		do {
			a = gen.nextInt(10);
			count++;
		} while (a < 5);
	}

	public ArrayList<String> getchoices() {
		for (int a = 0; a < count; a++) {
			choices.add(options[gen.nextInt(7)]);
		}
		return choices;
	}

	public ArrayList<Integer> getnum() {
		for (int a = 0; a < count; a++) {
			Integer e = gen.nextInt(6) + 1;
			number.add(e);
		}
		return number;
	}

	public ArrayList<String> getPartID(String Partid) {
		for (int b = 0; b < count; b++) {
			int r = gen.nextInt(10);
			String e;
			if (r < 4) {
				e = "";
			} else {
				e = Integer.toString(r);
			}
			String fin = Partid + e;
			partID.add(fin);
		}
		return partID;
	}

	public ArrayList<String> getavailability() {
		int t;
		for (int w = 0; w < count; w++) {
			t = gen.nextInt(12) + 2;
			availability.add("Estimated delivery time: " + t + " days");
		}
		return availability;
	}

	public ArrayList<Integer> getDistance(double lat, double lon) {
		int radius = 3959;
		for (int f = 0; f < count; f++) {
			int a = gen.nextInt(10);
			boolean plusminus = gen.nextBoolean();
			if (!plusminus) {
				a = a * (-1);
			}
			int b = gen.nextInt(10);
			boolean pm = gen.nextBoolean();
			if (!pm) {
				b = b * (-1);
			}
			double dlat = Math.toRadians(a);
			double dlon = Math.toRadians(b);
			double d = Math.sin(dlat / 2) * Math.sin(dlat / 2)
					+ Math.cos(Math.toRadians(lat))
					* Math.cos(Math.toRadians(lat + a)) * Math.sin(dlon / 2)
					* Math.sin(dlon / 2);
			double c = 2 * Math.atan2(Math.sqrt(d), Math.sqrt(1 - d));
			double dist = radius * c;
			distance.add((int) dist);
		}

		return distance;

	}
}
