package practice;

import java.util.ArrayList;

public class MainAnt extends Object implements Values{
	String ID;
	ArrayList<String> text;
	ArrayList<String> pics;
	ArrayList<String> qr;
	double latitude;
	double longitude;
	String availability;
	
	public MainAnt(String id, ArrayList<String> t, ArrayList<String> p, ArrayList<String> q, double lat, double lng)
	{
		ID = id;
		text = t;
		pics = p;
		qr = q;
		latitude = lat;
		longitude = lng;
	}
	public void setAvail(String s)	{ availability = s;	}
	
	public String getAvail()			{return availability;}
	public String getID()				{return ID;		}
	public ArrayList<String> getText()	{return text;	}
	public ArrayList<String> getPics()	{return pics;	}
	public ArrayList<String> getQr()	{return qr;		}
	public double getLat()				{return latitude;	}
	public double getLng()				{return longitude;	}
	
}