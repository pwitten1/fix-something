package practice;

public interface Values {
	
	public static final String NET_TYPE_CODE = "Net type code";
	//Net Codes

	public final String TAG = "AntHive";
	public final int MAX_PICS = 4;
	public final int MAX_TEXT = 10;
	
	public final String PREFS_NAME = "PREFS_NAME";
	public final String PREFS_USER = "PREFS_USER";
	public final String PREFS_ID = "PREFS_ID";
	public final String PREFS_ACCESS="PREFS_ACCESS";
	public final String PREFS_INITIAL="PREFS_INITIAL";
	public final String PREFS_DATA = "PREFS_DATA";
	
	public final String CODE="CODE";
	public final String USER_AUTH = "USER_AUTH";
	public final String CHECK_PART = "CHECK_PART";
	public final String SELECT_PART = "SELECT_PART";
	public final String CHECK_MAINTENANCE = "CHECK_MAINTENANCE";
	public final String SELECT_MAINTENANCE = "SELECT_MAINTENANCE";
	public final String UPDATE = "UPDATE";
	
	public final String CODE_CONFIRM = "CODE_CONFIRM";
	public final String CODE_USER = "CODE_USER";
	public final String CODE_ID = "CODE_ID";
	
	public final String CODE_CHOICE = "CODE_CHOICE";
	public final String CODE_ISSUE = "CODE_ISSUE";
	public final String CODE_DATA = "CODE_DATA";

}
